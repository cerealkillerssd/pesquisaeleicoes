package com.mycompany.pesquisaeleicoes;

import java.io.Serializable;
import org.apache.ignite.cache.query.annotations.QuerySqlField;
/**
 *
 * @author ricardo
 */
public class Eleitorado implements Serializable {
    @QuerySqlField(index = true)
    private Integer cod_mun;    //codigo do municipio
    
    @QuerySqlField(index = true)
    private String uf;    //Unidade federal
    
    @QuerySqlField(index = true)
    private String municipio;
    
    @QuerySqlField(index = true)
    private int eleitorado_apto;
    
    @QuerySqlField(index = true)
    private int eleitorado_fem;
    
    @QuerySqlField(index = true)
    private int eleitorado_mas;
    
    @QuerySqlField(index = true)
    private int idade_nao_informada;  //Codigo do municipio
    
    @QuerySqlField(index = true)
    private int ano16;
    
    @QuerySqlField(index = true)
    private int ano17;   
    
    @QuerySqlField(index = true)
    private int ano18_20;  
    
    @QuerySqlField(index = true)
    private int ano21_24;
    
    @QuerySqlField(index = true)
    private int ano25_34;   
    
    @QuerySqlField(index = true)
    private int ano35_44;   
    
    @QuerySqlField(index = true)
    private int ano45_59;  
    
    @QuerySqlField(index = true)
    private int ano60_69;  
    
    @QuerySqlField(index = true)
    private int ano70_79;   
    
    @QuerySqlField(index = true)
    private int maior79;   

    @QuerySqlField(index = true)
    private int invalido;
    public Integer getCod_mun() {
        return cod_mun;
    }

    public String getUf() {
        return uf;
    }

    public String getMunicipio() {
        return municipio;
    }

    public int getEleitorado_apto() {
        return eleitorado_apto;
    }

    public int getEleitorado_fem() {
        return eleitorado_fem;
    }

    public int getEleitorado_mas() {
        return eleitorado_mas;
    }

    public int getIdade_nao_informada() {
        return idade_nao_informada;
    }

    public int getAno16() {
        return ano16;
    }

    public int getAno17() {
        return ano17;
    }

    public int getAno18_20() {
        return ano18_20;
    }

    public int getAno21_24() {
        return ano21_24;
    }

    public int getAno25_34() {
        return ano25_34;
    }

    public int getAno35_44() {
        return ano35_44;
    }

    public int getAno45_59() {
        return ano45_59;
    }

    public int getAno60_69() {
        return ano60_69;
    }

    public int getAno70_79() {
        return ano70_79;
    }

    public int getMaior79() {
        return maior79;
    }

    public int getInvalido() {
        return invalido;
    }
    
    public void setCod_mun(Integer cod_mun) {
        this.cod_mun = cod_mun;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public void setEleitorado_apto(int eleitorado_apto) {
        this.eleitorado_apto = eleitorado_apto;
    }

    public void setEleitorado_fem(int eleitorado_fem) {
        this.eleitorado_fem = eleitorado_fem;
    }

    public void setEleitorado_mas(int eleitorado_mas) {
        this.eleitorado_mas = eleitorado_mas;
    }

    public void setIdade_nao_informada(int idade_nao_informada) {
        this.idade_nao_informada = idade_nao_informada;
    }

    public void setAno16(int ano16) {
        this.ano16 = ano16;
    }

    public void setAno17(int ano17) {
        this.ano17 = ano17;
    }

    public void setAno18_20(int ano18_20) {
        this.ano18_20 = ano18_20;
    }

    public void setAno21_24(int ano21_24) {
        this.ano21_24 = ano21_24;
    }

    public void setAno25_34(int ano25_34) {
        this.ano25_34 = ano25_34;
    }

    public void setAno35_44(int ano35_44) {
        this.ano35_44 = ano35_44;
    }

    public void setAno45_59(int ano45_59) {
        this.ano45_59 = ano45_59;
    }

    public void setAno60_69(int ano60_69) {
        this.ano60_69 = ano60_69;
    }

    public void setAno70_79(int ano70_79) {
        this.ano70_79 = ano70_79;
    }

    public void setMaior79(int maior79) {
        this.maior79 = maior79;
    }  

    public void setInvalido(int invalido) {
        this.invalido = invalido;
    }
}
