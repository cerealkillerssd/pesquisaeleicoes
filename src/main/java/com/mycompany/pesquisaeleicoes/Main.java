package com.mycompany.pesquisaeleicoes;

import java.util.List;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.CacheConfiguration;

/**
 *
 * @author ricardo
 */
public class Main {
    public static void main(String args[]) {
        float init;
        float end;
        
        init = System.nanoTime();
        try(Ignite ignite = Ignition.start()) {
            CacheConfiguration<Integer, Eleitorado> ecfg = new CacheConfiguration<>("eleitorado");
            ecfg.setIndexedTypes(Integer.class, Eleitorado.class);
            IgniteCache<Integer, Eleitorado> eleitorado = ignite.getOrCreateCache(ecfg);
            
            Populator populator = new Populator(eleitorado, ignite);
            System.out.println("Populando o banco de dados");
            populator.populateEleitorado();
            System.out.println("Finalizada a população");
            
            SqlFieldsQuery query = new SqlFieldsQuery(
                "SELECT	Eleitorado.uf, SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano16)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano17)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano18_20)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano21_24)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano25_34)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano35_44)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano45_59)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano60_69)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.ano70_79)/SUM(Eleitorado.eleitorado_apto), " +
                "100*SUM(Eleitorado.maior79)/SUM(Eleitorado.eleitorado_apto) " +
                "FROM Eleitorado " +
                "GROUP BY Eleitorado.uf " +
                "ORDER BY Eleitorado.uf;"
            );

            float inicio = System.nanoTime();
            List<List<?>> resultado = eleitorado.query(query).getAll();
            float fim = System.nanoTime();

            System.out.println("============================================================");
            System.out.println("Resultado da pesquisa:");
            System.out.println(resultado);
            System.out.println("Tempo demorado: " + (fim - inicio)/1000000 + "ms");
            System.out.println("Tempo demorado: " + (fim - inicio) + "ns");
            System.out.println("============================================================");
        }
        end = System.nanoTime();
        System.out.println("Pesquisas finalizadas");
        System.out.println("Tempo total: " + (end-init)/1000000000);      
    }
}
