package com.mycompany.pesquisaeleicoes;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;

/**
 *
 * @author ricardo
 */
public class Populator {
    private final IgniteCache<Integer, Eleitorado> ecache;
    private final Ignite ignite;
    
    public Populator(IgniteCache<Integer, Eleitorado> ecache, Ignite ignite) {
        this.ignite = ignite;
        this.ecache = ecache;
    }
    
    public void populateEleitorado() {
        Charset charset = Charset.forName("ISO-8859-1");
        
        List<Eleitorado> eleitList = new ArrayList<Eleitorado>();
        Path path = FileSystems.getDefault().getPath("Eleitorados", "Eleitorado.csv");

        try (BufferedReader reader = Files.newBufferedReader(path, charset)) {
            String line = reader.readLine();
            line = reader.readLine();
            int j = 0;
            int i;
            while (line != null) {
                Eleitorado aux = new Eleitorado();
                i = 0;
                String[] tokens = line.split(",");
                for (String dado : tokens){
                    if(dado.isEmpty()){
                        dado = "0";
                    }

                    switch(i){
                        case 0:
                            aux.setCod_mun(Integer.parseInt(dado));
                            break;
                        case 1:
                            aux.setUf(dado);
                            break;
                        case 2:
                            aux.setMunicipio(dado);
                            break;
                        case 3:
                            aux.setEleitorado_apto(Integer.parseInt(dado));
                            break;
                        case 4:
                            aux.setEleitorado_fem(Integer.parseInt(dado));
                            break;
                        case 5:
                            aux.setEleitorado_mas(Integer.parseInt(dado));
                            break;
                        case 6:
                            aux.setIdade_nao_informada(Integer.parseInt(dado));
                            break;
                        case 7:
                            aux.setAno16(Integer.parseInt(dado));
                            break;
                        case 8:
                            aux.setAno17(Integer.parseInt(dado));
                            break;
                        case 9:
                            aux.setAno18_20(Integer.parseInt(dado));
                            break;
                        case 10:
                            aux.setAno21_24(Integer.parseInt(dado));
                            break;
                        case 11:
                            aux.setAno25_34(Integer.parseInt(dado));
                            break;
                        case 12:
                            aux.setAno35_44(Integer.parseInt(dado));
                            break;
                        case 13:
                            aux.setAno45_59(Integer.parseInt(dado));
                            break;
                        case 14:
                            aux.setAno60_69(Integer.parseInt(dado));
                            break;
                        case 15:
                            aux.setAno70_79(Integer.parseInt(dado));
                            break;
                        case 16:
                            aux.setMaior79(Integer.parseInt(dado));
                            break;
                        case 17:
                            aux.setInvalido(Integer.parseInt(dado));
                            break;
                    }
                    i++;
                }
                eleitList.add(aux);
                j++;
                line = reader.readLine();
            }
        } catch (IOException erro) {
            System.err.format("IOException: %s%n", erro);
        }      
        
        //Coloca no cache do Ignite
        for(Eleitorado eleitorado: eleitList) {
            ecache.put(eleitorado.getCod_mun(), eleitorado);
        }
    }
}
